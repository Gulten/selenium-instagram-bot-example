from instagramUserInfo import username, password
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os

class Instagram:
    def __init__(self, username, password):
        self.browserProfile = webdriver.ChromeOptions()
        self.browserProfile.add_experimental_option('prefs', {'intl.accept_languages':'en,en_US'})
        os.chmod('/driver/path/chromedriver', 755)
        self.browser = webdriver.Chrome(executable_path='/driver/path/chromedriver', chrome_options=self.browserProfile)
        self.username = username
        self.password = password

    def signIn(self):
        self.browser.get('https://www.instagram.com/')
        time.sleep(3)
        usernameInput = self.browser.find_element_by_xpath('//*[@id="loginForm"]/div/div[1]/div/label/input')
        passwordInput = self.browser.find_element_by_xpath('//*[@id="loginForm"]/div/div[2]/div/label/input')

        usernameInput.send_keys(self.username)
        passwordInput.send_keys(self.password)
        passwordInput.send_keys(Keys.ENTER)

        time.sleep(3)

    def getFollowers(self):
        self.browser.get(f'https://www.instagram.com/{self.username}')
        followersLink = self.browser.find_element_by_xpath('//*[@id="react-root"]/section/main/div/header/section/ul/li[2]/a')
        followersLink.click()
        time.sleep(3)
        
        dialog = self.browser.find_element_by_css_selector("div[role=dialog] ul")
        followerCount = len(dialog.find_elements_by_css_selector('li'))

        print(f'first count: {followerCount}')

        action = webdriver.ActionChains(self.browser)

        while True:
            dialog.click()
            action.key_down(Keys.SPACE).key_up(Keys.SPACE).perform()
            time.sleep(2)

            newCount = len(dialog.find_elements_by_css_selector('li'))

            if followerCount != newCount:
                followerCount = newCount
                print(f"new count: {newCount}")
                time.sleep(1)
                pass
            else:
                break

        followers = dialog.find_elements_by_css_selector('li')

        time.sleep(2)

        followerList = []
        for user in followers:
            link = user.find_element_by_css_selector('a').get_attribute('href')
            followerList.append(link)

        with open("followers.txt", "w", encoding="UTF-8") as file:
            for item in followerList:
                file.write(item + "\n")     

    def followUser(self, username):
        self.browser.get(f'https://www.instagram.com/{username}')
        time.sleep(3)

        followButton = self.browser.find_element_by_tag_name("button")
        if followButton.text == "Follow":
            #followButton.click()
            print("Takip Etmiyorsunuz")
            time.sleep(2)
        else:    
            print("Zaten Takiptesiniz")
            time.sleep(2)

    def unFollowUser(self, username):
        self.browser.get(f'https://www.instagram.com/{username}')
        time.sleep(3)

        followButton = self.browser.find_element_by_xpath('//*[@id="react-root"]/section/main/div/header/section/div[1]/div[1]/div/div[2]/div/span/span[1]/button')
        if followButton.text == "Message":
            #followButton.click()
            time.sleep(2)
            self.browser.find_element_by_xpath('//button[text()="Unfollow"]').click()
        else:
            print("Zaten Takip Etmiyorsunuz")
            time.sleep(2)

instagram = Instagram(username, password)
instagram.signIn()
instagram.getFollowers()
#instagram.followUser('hesap1')

#list = ["hesap1", "hesap2"]

#for user in list:
#    instagram.followUser()
#    time.sleep(3)

instagram.browser.quit()
